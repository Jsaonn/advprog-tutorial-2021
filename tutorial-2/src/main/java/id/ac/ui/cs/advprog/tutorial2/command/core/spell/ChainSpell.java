package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> listSpell;

    public ChainSpell(ArrayList<Spell> listSpell) {
        this.listSpell = listSpell;
    }

    public void cast() {
        for(int i = 0; i < listSpell.size(); i++) {
            listSpell.get(i).cast();
        }
    }

    public void undo() {
        for(int i = listSpell.size()-1; i >=0; i--) {
            listSpell.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
