package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MenuTest {
    private Menu menu;

    @BeforeEach
    public void setUp() {
        menu = new SnevnezhaShirataki("Anjay");
    }

    @Test
    public void testMenuGetNameMethodReturn() throws Exception {
        assertEquals("Anjay", menu.getName());
    }

    @Test
    public void testMenuGetNoodleMethodReturn() throws Exception {
        assertNotNull(menu.getNoodle());
    }

    @Test
    public void testMenuGetMeatMethodReturn() throws Exception {
        assertNotNull(menu.getMeat());
    }

    @Test
    public void testMenuGetToppingMethodReturn() throws Exception {
        assertNotNull(menu.getTopping());
    }

    @Test
    public void testMenuGetFlavorMethodReturn() throws Exception {
        assertNotNull(menu.getFlavor());
    }

    @Test 
    public void testMenuCreateMenuInuzumaRamen() throws Exception {
        InuzumaRamen inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");
        assertNotNull(inuzumaRamen);
    }

    @Test 
    public void testMenuCreateMenuLiyuanSoba() throws Exception {
        LiyuanSoba liyuanSoba = new LiyuanSoba("Liyuan Soba");
        assertNotNull(liyuanSoba);
    }

    @Test 
    public void testMenuCreateMenuMondoUdon() throws Exception {
        MondoUdon mondoUdon = new MondoUdon("Mondo Udon");
        assertNotNull(mondoUdon);
    }

    @Test 
    public void testMenuCreateMenuSnevnezhaShirataki() throws Exception {
        SnevnezhaShirataki snevnezhaShirataki = new SnevnezhaShirataki("Snevnezha Shirataki");
        assertNotNull(snevnezhaShirataki);
    }
}