package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MeatTest {
    @Test
    public void testMeatBeefGetDescriptionMethodReturn() throws Exception {
        Beef beef = new Beef();
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }

    @Test
    public void testMeatChickenGetDescriptionMethodReturn() throws Exception {
        Chicken chicken = new Chicken();
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }

    @Test
    public void testMeatFishGetDescriptionMethodReturn() throws Exception {
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }

    @Test
    public void testMeatPorkGetDescriptionMethodReturn() throws Exception {
        Pork pork = new Pork();
        assertEquals("Adding Tian Xu Pork Meat...", pork.getDescription());
    }
}