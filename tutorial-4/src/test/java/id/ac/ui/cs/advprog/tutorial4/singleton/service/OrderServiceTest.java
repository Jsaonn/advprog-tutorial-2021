package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class OrderServiceTest {
    OrderServiceImpl orderService;

    @BeforeEach
    public void setUp() {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testSingletonOrderServiceImplOrderADrinkMethod() throws Exception {
        orderService.orderADrink("amer");
        assertEquals("amer", orderService.getDrink().getDrink());
    }

    @Test
    public void testSingletonOrderServiceImplOrderADrinkMethodError() throws Exception {
        orderService.orderADrink("amer");
        assertNotEquals("susu", orderService.getDrink().getDrink());
    }

    @Test
    public void testSingletonOrderServiceImplOrderAFoodMethod() throws Exception {
        orderService.orderAFood("mi goreng");
        assertEquals("mi goreng", orderService.getFood().getFood());
    }

    @Test
    public void testSingletonOrderServiceImplOrderAFoodMethodError() throws Exception {
        orderService.orderAFood("mi goreng");
        assertNotEquals("mi rebus", orderService.getFood().getFood());
    }

    @Test
    public void testSingletonOrderServiceImplGetDrinkMethod() throws Exception {
        assertNotNull(orderService.getDrink());
    }
    
    @Test
    public void testSingletonOrderServiceImplGetFoodMethod() throws Exception {
        assertNotNull(orderService.getFood());
    }
}