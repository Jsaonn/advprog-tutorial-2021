package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MenuServiceTest {
    private MenuServiceImpl menuService;
    private MenuServiceImpl menuServiceImpl;
    
    @BeforeEach
    public void setUp() {
        menuService = new MenuServiceImpl();
        menuServiceImpl = new MenuServiceImpl(new MenuRepository());
    }
    
    @Test
    public void testMenuServiceImplCreateMenuMethod() throws Exception {
        menuService.createMenu("WanPlus Beef Mushroom Soba", "LiyuanSoba");
        menuService.createMenu("Bakufu Spicy Pork Ramen", "InuzumaRamen");
        menuService.createMenu("Good Hunter Cheese Chicken Udon", "MondoUdon");
        menuService.createMenu("Morepeko Flower Fish Shirataki", "SnevnezhaShirataki");
    }

    @Test
    public void testMenuServiceImplGetMenusMethodReturn() throws Exception {
        assertNotNull(menuService.getMenus());
    }
}