# Lazy vs Eager Instantiation
---

### Penjelasan

Dalam Singleton Design Pattern yang dipelajari di kelas, terdapat 2 jenis **instantiation**, yaitu **lazy instantiation** dan **eager instantiation**. 

Didalam **Lazy Instantiation**, object hanya dibuat ketika object itu diperlukan. Dalam *OrderDrink.java*, object OrderDrink dibuat tapi dengan value *null*, sehingga nanti kita yang akan membuat objectnya ketika objectnya diperlukan.

Sedangkan, didalam **Eager Instantiation**, object dibuat dari awal, sehingga ketika dibutuhkan kita hanya perlu untuk memanggil method getter untuk mendapatkan instancenya. Dalam *OrderFood.java*, object OrderFood dibuat dari awal, sehingga kita bisa memanggilnya langsung dengan method *getInstance*


### Kelebihan dan kekurangan

##### Lazy Instantiation
Kelebihan :
1. Object dibuat hanya ketika dibutuhkan, sehingga tidak menguras CPU Time saat compiling
2. Dapat melakukan handling terhadap Exception didalam method

Kekurangan :
1. Harus mengecek kondisi null atau tidak
2. Tidak bisa mengakses instance secara langsung, harus buat method dulu


##### Eager Instantiation
Kelebihan :
1. Sangat mudah diimplement
2. Tidak terlalu memakan waktu CPU Time

Kekurangan :
1. Memakan resource, karena instance selalu dibuat walaupun tidak digunakan
2. Tidak bisa melakukan handling terhadap Exception didalam method
