package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private BowRepository bowRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private WeaponRepository weaponRepository;

    private boolean isInit = false;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if(!isInit) {
            List<Bow> listBow = bowRepository.findAll();
            for(Bow bow : listBow) {
                BowAdapter bowAdapter = new BowAdapter(bow);
                weaponRepository.save(bowAdapter);
            }
    
            List<Spellbook> listSpellbook = spellbookRepository.findAll();
            for(Spellbook spellbook : listSpellbook) {
                SpellbookAdapter spellbookAdapter = new SpellbookAdapter(spellbook);
                weaponRepository.save(spellbookAdapter);
            }

            isInit = true;
        }

        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(attackType == 0) {
            String atk = weapon.normalAttack();
            String nm = weapon.getHolderName();
            String wnm = weapon.getName();
            logRepository.addLog(nm + " attacked with " + wnm + " (normal attack): " + atk);
        } else {
            String atk = weapon.chargedAttack();
            String nm = weapon.getHolderName();
            String wnm = weapon.getName();
            logRepository.addLog(nm + " attacked with " + wnm + " (charged attack): " + atk);
        }
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
