package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean isChargeReady;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.isChargeReady = true;
    }

    @Override
    public String normalAttack() {
        this.isChargeReady = true;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(this.isChargeReady) {
            this.isChargeReady = false;
            return spellbook.largeSpell();
        } else {
            return "Large Spell is not ready!";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
