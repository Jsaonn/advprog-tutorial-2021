package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini melakukan transformasi dengan Caesar Cipher shift 13
 */

 public class BukanTransformation {
    private static int shift;

    public BukanTransformation() {
        this.shift = 13;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int codexSize = codex.getCharSize();
        char[] result = new char[text.length()];

        for(int i=0; i < text.length(); i++) {
            char oldChar = text.charAt(i);
            int charIdx = codex.getIndex(oldChar);
            int newCharIdx = (charIdx + (selector * shift));
            newCharIdx = newCharIdx < 0 ? codexSize + newCharIdx : newCharIdx % codexSize;
            result[i] = codex.getChar(newCharIdx);
        }

        return new Spell(new String(result), codex);
    }
 }